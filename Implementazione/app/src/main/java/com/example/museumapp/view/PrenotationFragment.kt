package com.example.museumapp.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.museumapp.R
import com.example.museumapp.databinding.FragmentTicketBinding
import com.example.museumapp.util.SingletonNFC
import com.example.museumapp.util.fromTimestapToString
import com.example.museumapp.viewModel.PrenotationViewModel
import java.util.*


class PrenotationFragment : Fragment() {

    private lateinit var prenotationViewModel: PrenotationViewModel

    private var _binding: FragmentTicketBinding? = null
    private val binding get() = _binding!!

    private var picker: DatePickerDialog? = null
    private lateinit var popupMenuEnding: PopupMenu
    private lateinit var popupMenuStarting: PopupMenu


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentTicketBinding.inflate(inflater, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        prenotationViewModel =
            ViewModelProviders.of(this).get(PrenotationViewModel::class.java)

        prenotationViewModel.prenotazione.observe(viewLifecycleOwner, Observer {

            binding.cardLayout.isVisible = true
            binding.noTicketImg.isVisible = false
            binding.cardOverlayedLayout.isVisible = false
            binding.plusBtn.isVisible = false
            binding.closeBtn.isVisible = false

            binding.qrCodeImg.setImageBitmap(it.qrCode)
            binding.dateTicketLbl.text = fromTimestapToString(it.date!!)
            binding.startingHourTicketLbl.text = it.timeSlot?.startingHour
            binding.endingHourTicketLbl.text = it.timeSlot?.endingHour
        })

        SingletonNFC.instance.nFCData.observe(viewLifecycleOwner, Observer {
            if (SingletonNFC.instance.nFCEnable) {
                Toast.makeText(activity, "Tag scanned, please open the home tab", Toast.LENGTH_LONG).show()
            }
        })

        setPopUpMenu()
        setListener()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        //TODO remove
        _binding = null
    }


    private fun onClickClose() {

        val aniFadeOut: Animation = AnimationUtils.loadAnimation(
            this.requireContext(),
            R.anim.slide_out
        )
        binding.cardLayout.startAnimation(aniFadeOut)
        binding.cardOverlayedLayout.startAnimation(aniFadeOut)

        binding.cardLayout.postOnAnimation {
            binding.cardLayout.isVisible = false
            binding.cardOverlayedLayout.isVisible = false
            binding.plusBtn.isVisible = true
        }

    }


    private fun onClickBook() {

        if(!isInputCorrect()) return

        binding.bookBtn.isEnabled = false

        prenotationViewModel.addPrenotazione(
            binding.dateLbl.text.toString(),
            binding.startingHourLbl.text as String,
            binding.endingHourLbl.text as String
        ){
            if(!it)  binding.bookBtn.isEnabled = true
        }
    }


    private fun onClickEndingHour() {

        popupMenuEnding.setOnMenuItemClickListener { item ->
            binding.endingHourLbl.text = item.title
            true
        }

        popupMenuEnding.show()
    }


    private fun onClickStartingHour() {

        popupMenuStarting.setOnMenuItemClickListener { item ->

            binding.endingHourLbl.text = ""
            binding.startingHourLbl.text = item.title
            popupMenuEnding.menu.removeGroup(1)

            var startH = item.title.toString().toInt()

            //Fill popUp menu
            while (startH<19 && prenotationViewModel.validiEndingHours.contains(startH + 1)){
                popupMenuEnding.menu.add(1, 0, 0, (startH+1).toString())
                startH++
            }
            true
        }
        popupMenuStarting.show()
    }


    private fun onClickPickDate() {

        val cldr: Calendar = Calendar.getInstance()
        val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
        val month: Int = cldr.get(Calendar.MONTH)
        val year: Int = cldr.get(Calendar.YEAR)

        picker = this.context?.let {
            DatePickerDialog(it, { _, year, monthOfYear, dayOfMonth ->

                binding.endingHourLbl.text = ""
                binding.startingHourLbl.text = ""
                popupMenuEnding.menu.removeGroup(1)
                popupMenuStarting.menu.removeGroup(1)

                val newFormatMonth: String = if(monthOfYear < 9){
                    "0" + (monthOfYear+1).toString()
                }else{
                    (monthOfYear+1).toString()
                }

                prenotationViewModel.getAvailableData("$dayOfMonth/$newFormatMonth/$year") { isEmpty ->

                    if(isEmpty){
                        Toast.makeText(context, "Selected date is full", Toast.LENGTH_LONG).show()
                    }else{
                        //Fill popUp menu
                        for(i in prenotationViewModel.validStartingHours){
                            popupMenuStarting.menu.add(1, 0, 0, i.toString())
                        }
                        binding.dateLbl.text = "$dayOfMonth/$newFormatMonth/$year"
                    }
                }
            }, year, month, day) }

        picker?.datePicker?.minDate = System.currentTimeMillis() - 1000
        picker?.show()
    }


    private fun onClickPlus() {
        binding.cardLayout.isVisible = true
        binding.cardOverlayedLayout.isVisible = true
        binding.plusBtn.isVisible = false

        val aniFadeIn: Animation = AnimationUtils.loadAnimation(
            this.requireContext(),
            R.anim.slide_in
        )
        binding.cardLayout.startAnimation(aniFadeIn)
        binding.cardOverlayedLayout.startAnimation(aniFadeIn)
    }


    private fun isInputCorrect(): Boolean{

        if(binding.dateLbl.text.isEmpty() || binding.startingHourLbl.text.isEmpty() || binding.endingHourLbl.text.isEmpty()){
            Toast.makeText(this.context, "Fill all the fields before booking", Toast.LENGTH_LONG).show()
            return false
        }

        val startH = binding.startingHourLbl.text.toString().toInt()
        val endH = binding.endingHourLbl.text.toString().toInt()

        if(startH >= endH){
            Toast.makeText(this.context, "Select a valid entry", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }


    private fun setListener(){
        binding.plusBtn.setOnClickListener { onClickPlus() }
        binding.pickDateBtn.setOnClickListener { onClickPickDate() }
        binding.pickStartBtn.setOnClickListener { onClickStartingHour() }
        binding.pickEndBtn.setOnClickListener{ onClickEndingHour() }
        binding.bookBtn.setOnClickListener{ onClickBook() }
        binding.closeBtn.setOnClickListener{ onClickClose() }
    }


    private fun setPopUpMenu(){
        popupMenuEnding = PopupMenu(this.context, binding.pickEndBtn)
        popupMenuEnding.menuInflater.inflate(R.menu.pop_up_menu_ending_hour, popupMenuEnding.menu)
        popupMenuStarting = PopupMenu(this.context, binding.pickStartBtn)
        popupMenuStarting.menuInflater.inflate(R.menu.popup_menu_starting_hour, popupMenuStarting.menu)
    }



}

