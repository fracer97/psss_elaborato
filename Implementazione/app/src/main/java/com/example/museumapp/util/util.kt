package com.example.museumapp.util

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix


internal fun fromTimestapToString(timeStamp: Timestamp): String {

    var date = java.time.format.DateTimeFormatter.ISO_INSTANT.format(
        java.time.Instant.ofEpochSecond(timeStamp.seconds)
    )
    date = date.subSequence(0, 10).toString()

    Log.d("DAV", date)

    val splittedDate = date.split("-").toTypedArray()

    date = splittedDate[2] + "-" + splittedDate[1] + "-" + splittedDate[0]
    Log.d("DAV", date)

    return date
}


@Throws(WriterException::class, NullPointerException::class)
internal fun textToImage(text: String, width: Int, height: Int): Bitmap? {

    val bitMatrix: BitMatrix = try {
        MultiFormatWriter().encode(
            text, BarcodeFormat.QR_CODE,
            width, height, null
        )
    } catch (Illegalargumentexception: IllegalArgumentException) {
        return null
    }

    val bitMatrixWidth = bitMatrix.width
    val bitMatrixHeight = bitMatrix.height
    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)
    val colorWhite = -0x1
    val colorBlack = -0x1000000

    for (y in 0 until bitMatrixHeight) {
        val offset = y * bitMatrixWidth
        for (x in 0 until bitMatrixWidth) {
            pixels[offset + x] = if (bitMatrix[x, y]) colorBlack else colorWhite
        }
    }

    val bitmap =
        Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)
    bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight)

    return bitmap
}


class SingletonNFC private constructor() {

    private object HOLDER {
        val INSTANCE = SingletonNFC()
    }

    var nFCEnable = true
    val nFCData  = MutableLiveData<String>()

    companion object {
        val instance: SingletonNFC by lazy { HOLDER.INSTANCE }
    }
}