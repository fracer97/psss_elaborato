package com.example.museumapp.view.customRecycleView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_view.view.*
import com.example.museumapp.R
import com.squareup.picasso.Picasso

class CustomAdapter(
    private val data: List<DataObject>
): RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    private val items: MutableList<CardView>


    init {
        this.items = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view,parent,false)

        return ViewHolder(
            v
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = data[position].title
        Picasso.get().load(data[position].imgURL).into(holder.image)

        items.add(holder.card)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    class ViewHolder
    internal constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){
        val title: TextView = itemView.title
        var image: ImageView = itemView.paintingImageView
        val card: CardView = itemView.card
    }
}
