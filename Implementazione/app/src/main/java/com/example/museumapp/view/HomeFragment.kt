package com.example.museumapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.museumapp.R
import com.example.museumapp.databinding.FragmentHomeBinding
import com.example.museumapp.util.SingletonNFC
import com.example.museumapp.viewModel.HomeViewModel
import com.example.museumapp.view.customRecycleView.CustomAdapter
import com.example.museumapp.view.customRecycleView.DataObject


class HomeFragment : Fragment() {

    private lateinit var horizontalRecycleView: RecyclerView
    private lateinit var homeViewModel: HomeViewModel

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.roomDetailedLayout.isGone = true
        binding.roomStatusLayout.isGone = false

        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)

        setListnerForRoomsStatus()

        SingletonNFC.instance.nFCData.observe(viewLifecycleOwner, Observer {
            onTagScanned(it)
        })

        binding.closeBtn.setOnClickListener{ onClickClose() }
    }


    override fun onDestroyView() {
        super.onDestroyView()

        homeViewModel.detachListner()
        _binding = null
    }


    private fun onClickClose() {
        removeDetailedRoomLayout()
    }


    private fun onTagScanned(nFCPayload: String){

        val idSala = nFCPayload[0]
        val nFCType = nFCPayload.substring(2, nFCPayload.length)

        Log.d("NFC", nFCType)

        homeViewModel.onTagScanned(idSala.toString(), nFCType) {
            when (it) {
                "access" -> {
                    populateRecycleView()
                }
                "full" -> {
                    Toast.makeText(
                        activity,
                        "Room has reached full capacity, please wait to enter",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else -> { //Exit
                    if(binding.roomDetailedLayout.isVisible)
                        removeDetailedRoomLayout()
                }
            }
        }
    }


    private fun populateRecycleView(){

        if (homeViewModel.salaVisitata.quadri == null){
            return
        }

        val paintings = homeViewModel.salaVisitata.quadri!!

        binding.textView9.text = getString(R.string.titleRoom, homeViewModel.salaVisitata.id)
        binding.roomInfoLbl.text = homeViewModel.salaVisitata.theme
        horizontalRecycleView = binding.horizontalRecycleView

        val data: MutableList<DataObject> = ArrayList()
        val layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)

        for(painting in paintings){
            data.add(DataObject(painting.title, painting.imgURL!!))
        }
        val adapter = CustomAdapter(data)

        horizontalRecycleView.layoutManager = layoutManager
        horizontalRecycleView.setHasFixedSize(true)
        horizontalRecycleView.adapter = adapter

        displayDetailedRoomLayout()
    }


    private fun removeDetailedRoomLayout(){

        val aniFadeIn: Animation = AnimationUtils.loadAnimation(
            this.requireContext(),
            R.anim.fade_in
        )
        val aniSlideOut: Animation = AnimationUtils.loadAnimation(
            this.requireContext(),
            R.anim.slide_out
        )

        binding.roomDetailedLayout.startAnimation(aniSlideOut)
        binding.roomStatusLayout.startAnimation(aniFadeIn)

        binding.roomDetailedLayout.postOnAnimation {
            binding.roomDetailedLayout.isGone = true
            binding.roomStatusLayout.isGone = false
        }
    }


    private fun displayDetailedRoomLayout(){

        val aniFadeOut: Animation = AnimationUtils.loadAnimation(
            this.requireContext(),
            R.anim.fade_out
        )
        val aniSlideIn: Animation = AnimationUtils.loadAnimation(
            this.requireContext(),
            R.anim.slide_in
        )

        binding.roomDetailedLayout.startAnimation(aniSlideIn)
        binding.roomStatusLayout.startAnimation(aniFadeOut)

        binding.roomDetailedLayout.postOnAnimation {
            binding.roomDetailedLayout.isGone = false
            binding.roomStatusLayout.isGone = true
        }
    }


    private fun setListnerForRoomsStatus(){

        homeViewModel.getLiveRoomStatus()
        homeViewModel.statusRoom1.observe(viewLifecycleOwner, Observer {
            statusRoomManager(binding.counterRoom1, binding.statusRoom1, it.first, it.second)
        })
        homeViewModel.statusRoom2.observe(viewLifecycleOwner, Observer {
            statusRoomManager(binding.counterRoom2, binding.statusRoom2, it.first, it.second)
        })
        homeViewModel.statusRoom3.observe(viewLifecycleOwner, Observer {
            statusRoomManager(binding.counterRoom3, binding.statusRoom3, it.first, it.second)
        })
        homeViewModel.statusRoom4.observe(viewLifecycleOwner, Observer {
            statusRoomManager(binding.counterRoom4, binding.statusRoom4, it.first, it.second)
        })
        homeViewModel.statusRoom5.observe(viewLifecycleOwner, Observer {
            statusRoomManager(binding.counterRoom5, binding.statusRoom5, it.first, it.second)
        })
    }


    private fun statusRoomManager(counterRoom: TextView, statusRoomImg: ImageView, visitorNumber: String, capacity: String){

        counterRoom.text = getString(R.string.counterRoom, visitorNumber, capacity)

        if(visitorNumber == capacity){
            statusRoomImg.setImageResource(R.drawable.room_status_full)
        }else{
            statusRoomImg.setImageResource(R.drawable.room_status_vacant)
        }

    }
}
