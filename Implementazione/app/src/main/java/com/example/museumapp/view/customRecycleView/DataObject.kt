package com.example.museumapp.view.customRecycleView

data class DataObject(
    var title: String,
    var imgURL: String
)