package com.example.museumapp.model

data class TimeSlot(var startingHour: String, var endingHour: String)