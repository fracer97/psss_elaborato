package com.example.museumapp.model

data class Quadro (var title: String, var imgURL: String?, var author: String)