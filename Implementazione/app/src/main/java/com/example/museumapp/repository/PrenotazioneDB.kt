package com.example.museumapp.repository

import android.util.Log
import com.example.museumapp.util.fromTimestapToString
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PrenotazioneDB {
    companion object{

        //Database references
        private val dbRef = Firebase.firestore
        private val prenotazioniRef = dbRef.collection("prenotazioni")
        private val dateRef = dbRef.collection("date")


        internal fun addPrenotation(prenotationDoc: HashMap<String, Comparable<*>?>, idPrenotazione: String, onResult: (Boolean) -> Unit){

            prenotazioniRef.document(idPrenotazione)
                .set(prenotationDoc)
                .addOnSuccessListener {
                    Log.d("DB_F -> addPrenotation", "set ok")
                    incrementPrenotation(
                        fromTimestapToString(prenotationDoc["date"] as Timestamp),
                        prenotationDoc["startingHour"] as String,
                        prenotationDoc["endingHour"] as String
                    )
                    onResult(true)
                }
                .addOnFailureListener {
                    Log.w("DB_F -> addPrenotation", "error performing query", it)
                }
        }


        private fun incrementPrenotation(prenotationDate: String, startingHour: String, endingHour: String){

            val startingHourInt = startingHour.toInt()
            val endingHourInt = endingHour.toInt()

            for (i in startingHourInt until endingHourInt){
                dateRef.document(prenotationDate)
                    .update(i.toString(), FieldValue.increment(1))
            }
        }


        internal fun getPrenotazione(onResult: (ArrayList<QueryDocumentSnapshot>?) -> Unit) {

            val currentTimestamp = Timestamp(Date())
            var firebaseTimestamp: Timestamp
            val validResults = ArrayList<QueryDocumentSnapshot>()

            prenotazioniRef.whereEqualTo("idUtente","0001")
                .get()
                .addOnSuccessListener { documents ->
                    Log.d("DB_F -> getPrenotazione", documents.toString())

                    for(document in documents){

                        firebaseTimestamp = document["date"] as Timestamp

                        if(currentTimestamp < firebaseTimestamp){
                            validResults.add(document)
                        }
                    }
                    onResult(validResults)
                }
                .addOnFailureListener {
                    Log.w("DB_F -> getPrenotazione", "error performing query")
                }
        }


        internal fun getAvailableHours(selectedData: String, onResult: (QuerySnapshot?) -> Unit){

            val newFormatData = selectedData.replace("/","-")

            dateRef.whereEqualTo(FieldPath.documentId(), newFormatData)
                .get()
                .addOnSuccessListener {
                    onResult(it)
                }
                .addOnFailureListener() {
                    Log.d("DB_F", "Error")
                }
        }
    }
}

