package com.example.museumapp.model


import kotlin.collections.ArrayList


data class Sala(
    var id: String,
    var theme: String,
    var capacity: Int,
    var quadri: ArrayList<Quadro>?,
    var visitorNumber: Int
) {
    constructor(id: String) : this(id, "", 0, null, 0){
        quadri = ArrayList<Quadro>()
    }

    constructor() : this("","",0,null,0){
        quadri = ArrayList<Quadro>()
    }
}