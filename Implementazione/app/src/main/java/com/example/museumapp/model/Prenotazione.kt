package com.example.museumapp.model

import android.graphics.Bitmap
import com.example.museumapp.util.textToImage
import com.google.firebase.Timestamp


enum class Status{
    new,
    ongoing,
    expired
}

data class Prenotazione(var id: String?,
                   var date: Timestamp?,
                   var timeSlot: TimeSlot?,
                   var qrCode: Bitmap?,
                   var status: Status?
) {
    constructor() : this(null,null,null,null,null)

    constructor(id: String, date: Timestamp, timeSlot: TimeSlot, status: Status) :
            this(id, date, timeSlot, null, status){
        qrCode = textToImage(id,100,100)
    }
}