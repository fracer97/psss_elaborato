package com.example.museumapp.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.museumapp.repository.PrenotazioneDB
import com.example.museumapp.model.Prenotazione
import com.example.museumapp.model.Status
import com.example.museumapp.model.TimeSlot
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.*

class PrenotationViewModel : ViewModel() {

    internal val validStartingHours = ArrayList<Int>()
    internal val validiEndingHours = ArrayList<Int>()

    internal var prenotazione = MutableLiveData<Prenotazione>().apply {
        getPrenotazione {
            if(it.id != null){
                value = it
            }
        }
    }


    internal fun getAvailableData(selectedDate: String, onCompletion:(Boolean) -> Unit){

        validiEndingHours.clear()
        validStartingHours.clear()

        PrenotazioneDB.getAvailableHours(selectedDate) { results ->

            if (results != null) {
                for(result in results){
                    for(i in 9 until 19){
                        if(result[i.toString()] != 90L){
                            validStartingHours.add(i)
                            validiEndingHours.add(i+1)
                        }
                    }
                }
                onCompletion(validStartingHours.isEmpty())
            }
        }
    }


    private fun getPrenotazione(onCompletion:(Prenotazione)->Unit){

        var foundPrenotazione = Prenotazione()

        PrenotazioneDB.getPrenotazione{ results ->
            if (results != null) {
                for (result in results) {

                    foundPrenotazione = Prenotazione(
                        result.id,
                        result["date"] as Timestamp,
                        TimeSlot(result["startingHour"] as String,result["endingHour"] as String),
                        Status.new //To be implemented
                    )
                }
                onCompletion(foundPrenotazione)
            }
        }
    }


    internal fun addPrenotazione(dataPrenotazione: String, startingHour: String, endingHour: String, onCompletion: (Boolean) -> Unit){

        val r = Random()
        val idPrenotazione = String.format("%04d", r.nextInt(1001))
        val dateFormat = SimpleDateFormat("d/M/yyyy HH:mm:ss", Locale.ITALY)
        val fasciaOraria = TimeSlot(startingHour, endingHour)

        dateFormat.timeZone = TimeZone.getDefault()

        val parsedDate = dateFormat.parse("$dataPrenotazione $endingHour:00:00")

        val newPrenotazione = Prenotazione(idPrenotazione, Timestamp(parsedDate!!), fasciaOraria, Status.new)

        val prenotationHashed = hashMapOf(
            "date" to newPrenotazione.date,
            "startingHour" to newPrenotazione.timeSlot!!.startingHour,
            "endingHour" to newPrenotazione.timeSlot!!.endingHour,
            "status" to newPrenotazione.status,
            "idUtente" to "0001"
        )

        PrenotazioneDB.addPrenotation(prenotationHashed, idPrenotazione){
            if(it){
                prenotazione.value = newPrenotazione
            }
            onCompletion(it)
        }
    }

}