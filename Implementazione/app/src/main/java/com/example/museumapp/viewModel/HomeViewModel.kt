package com.example.museumapp.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.museumapp.model.Quadro
import com.example.museumapp.model.Sala
import com.example.museumapp.repository.SalaDB
import com.example.museumapp.util.SingletonNFC

class HomeViewModel : ViewModel() {

    internal var salaVisitata = Sala()

    internal var statusRoom1 = MutableLiveData<Pair<String,String>>()
    internal var statusRoom2 = MutableLiveData<Pair<String,String>>()
    internal var statusRoom3 = MutableLiveData<Pair<String,String>>()
    internal var statusRoom4 = MutableLiveData<Pair<String,String>>()
    internal var statusRoom5 = MutableLiveData<Pair<String,String>>()


    internal fun onTagScanned(idSala: String, nFCType: String, onCompletion: (String) -> Unit){

        when (nFCType) {
            "access" -> {
                if(SingletonNFC.instance.nFCEnable){
                    SalaDB.incrementVisitorNumber(idSala) {
                        if (it){
                            salaVisitata = Sala(idSala)
                            this.getSalaInfo {
                                SingletonNFC.instance.nFCEnable = false
                                onCompletion("access")
                            }
                        }
                        else onCompletion("full")
                    }
                }
            }
            "exit" -> {
                if(!SingletonNFC.instance.nFCEnable && idSala == salaVisitata.id){
                    SalaDB.modifyVisitorNumberIn(idSala, false)
                    SingletonNFC.instance.nFCEnable = true
                    onCompletion("exit")
                }
            }
            else -> return
        }
    }


    internal fun getLiveRoomStatus(){
        SalaDB.getLiveRoomStatus { rooms->
            for(room in rooms){
                when(room.id){
                    "1" -> statusRoom1.value = Pair(room["visitorNumber"].toString(), room["capacity"].toString())
                    "2" -> statusRoom2.value = Pair(room["visitorNumber"].toString(), room["capacity"].toString())
                    "3" -> statusRoom3.value = Pair(room["visitorNumber"].toString(), room["capacity"].toString())
                    "4" -> statusRoom4.value = Pair(room["visitorNumber"].toString(), room["capacity"].toString())
                    "5" -> statusRoom5.value = Pair(room["visitorNumber"].toString(), room["capacity"].toString())
                }
            }
        }
    }


    private fun getSalaInfo(onCompletion: () -> Unit) {

        SalaDB.getSalaInfo(salaVisitata.id) { results->

            if(results!=null){
                for(result in results){
                    salaVisitata.theme = result["theme"] as String
                }
                this.getQuadriInfo {
                    onCompletion()
                }
            }
        }
    }


    private fun getQuadriInfo(onCompletion: () -> Unit) {

        SalaDB.getQuadriInfo(salaVisitata.id) { results ->

            if (results != null) {

                for (result in results) {
                    val painting = Quadro(
                        result["title"] as String,
                        result["URL"] as String,
                        result["author"] as String
                    )
                    salaVisitata.quadri!!.add(painting)
                }
                onCompletion()
            }
        }
    }


    internal fun detachListner(){
        SalaDB.detachListner()
    }
}