package com.example.museumapp.repository

import android.util.Log
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class SalaDB {
    companion object{

        private val dbRef = Firebase.firestore
        private val saleRef = dbRef.collection("sale")
        private val quadriRef = dbRef.collection("quadri")
        private lateinit var listner: ListenerRegistration


        internal fun getSalaInfo(idSala: String, onResult: (QuerySnapshot?) -> Unit) {
            Log.d("DB_F -> GetSale", "Ongoing")
            saleRef.whereEqualTo(FieldPath.documentId(), idSala)
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        Log.d("DB_F -> GetSale", "${document.id} => ${document.data}")
                    }
                    onResult(documents)
                }
                .addOnFailureListener {
                    Log.w("DB_F -> Get Sale", "Error")
                }
        }


        internal fun modifyVisitorNumberIn(idSala: String, isIncremented: Boolean){
            val value: Long = if (isIncremented) 1
            else -1
            saleRef.document(idSala).update("visitorNumber", FieldValue.increment(value))
        }


        internal fun incrementVisitorNumber(idSala: String, onResult: (Boolean) -> Unit){

            saleRef.document(idSala)
                .get()
                .addOnSuccessListener {
                    val visitorNumber = it["visitorNumber"] as Long
                    val capacity = it["capacity"] as Long

                    if (visitorNumber < capacity){
                        modifyVisitorNumberIn(idSala, true)
                        onResult(true)
                    }else onResult(false)
                }
                .addOnFailureListener { onResult(false) }
        }


        internal fun getQuadriInfo(idSala: String, onResult: (QuerySnapshot?) -> Unit) {

            quadriRef.whereEqualTo("idSala", idSala)
                .get()
                .addOnSuccessListener { documents ->
                    Log.d("DB_F -> getQuadriInfo", "query ok")
                    onResult(documents)
                }
                .addOnFailureListener {
                    Log.w("DB_F -> getQuadriInfo", "error performing query", it)
                }
        }


        internal fun getLiveRoomStatus(onResult: (ArrayList<QueryDocumentSnapshot>) -> Unit){

            val roomsModified = ArrayList<QueryDocumentSnapshot>()

            listner = saleRef.addSnapshotListener { snapshots, e ->
                if (e != null) {
                    Log.w("DB_F -> getLiveRoomStatus", "listen:error", e)
                    return@addSnapshotListener
                }

                for (dc in snapshots!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            Log.d("DB_F -> getLiveRoomStatus", "New room: ${dc.document.data}")
                            roomsModified.add(dc.document) //First time
                        }
                        DocumentChange.Type.MODIFIED -> {
                            Log.d("DB_F -> getLiveRoomStatus", "Modified room: ${dc.document.data}")
                            roomsModified.add(dc.document)  //On modify
                        }
                        DocumentChange.Type.REMOVED -> Log.d("DB_F -> getLiveRoomStatus", "Removed room: ${dc.document.data}")
                    }
                }
                onResult(roomsModified)
            }
        }


        internal fun detachListner(){ listner.remove() }

    }
}